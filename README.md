Xivo-solutions-doc
==================

This is the documentation project of the XiVO Solutions. It is available at http://xivosolutions.readthedocs.io/.

Dependencies
------------

* Sphinx (package python-sphinx on Debian)
* sphinx-git modified (for git changelog). To install it:

  pushd /tmp && sudo pip install git+https://gitlab.com/xivo.solutions/sphinx-git.git@tagtitles && popd

Build
-----

   make clean html


PDF version
-----------

You will need a LATEX compilation suite. On Debian, you can use the following
packages :

$ apt-get install texlive-latex-base texlive-latex-recommended
texlive-latex-extra texlive-fonts-recommended


