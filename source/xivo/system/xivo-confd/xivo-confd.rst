.. _xivo-confd:

==========
xivo-confd
==========

xivo-confd is a HTTP server that provides a RESTful API service for configuring and managing basic
resources on a XiVO server.

.. toctree::
   :maxdepth: 1

   Developer's Guide<developer>
