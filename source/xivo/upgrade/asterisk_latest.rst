.. _upgrade_asterisk_latest:

##########################
Upgrade to asterisk latest
##########################

Before integrating a new version of asterisk in *XiVO PBX*, we first build it and deliver it in the ``latest`` section of our
repository.

The goal is to make it available for specific cases (e.g. urgent bugfixes) before shipping it as the default version.
This page explains how to upgrade/downgrade to/from this latest version.

.. warning:: This is a specific procedure. You should know what you are doing.


To upgrade
**********

.. warning:: This will upgrade your asterisk version and trigger a *restart* of asterisk.
             You should know what you are doing.

#. Edit the xivo sources list file :file:`/etc/apt/sources.list.d/xivo-dist.list` and add the latest section 
   (note **latest** after ``main``)::

	deb http://mirror.xivo.solutions/archive/ xivo-VERSION-latest main latest

#. Update the package list::

	apt-get update

#. Check the candidate versions::

	apt-cache policy asterisk

#. Install the new version (install also ``asterisk-dbg`` package if applicable)::

	apt-get install asterisk

#. Restart the services (if you have a *XiVO CC*, you should restart its services too)::

    xivo-service restart


To downgrade
************

.. warning:: This will downgrade your asterisk version and trigger a *restart* of asterisk.
             You should know what you are doing.


#. Edit the xivo sources list file :file:`/etc/apt/sources.list.d/xivo-dist.list` and remove the latest section::

    deb http://mirror.xivo.solutions/archive/ xivo-VERSION-latest main

#. Update the sources list::
   
    apt-get update

#. Check the proposed versions with ``apt-cache policy asterisk``.
   For example it will give you the following result::
   
    # apt-cache policy asterisk
    asterisk:
      Installed: 8:13.13.1-1~xivo9+13.13.1+20170105.165406.d2ff9a5
      Candidate: 8:13.13.1-1~xivo9+13.13.1+20170105.165406.d2ff9a5
      Version table:
     *** 8:13.13.1-1~xivo9+13.13.1+20170105.165406.d2ff9a5 0
            100 /var/lib/dpkg/status
         8:13.10.0-1~xivo9+2017.01+master+20170208.085934.3b143b7 0
            500 http://mirror.xivo.solutions/archive/ xivo-2017.01-latest/main amd64 Packages

#. And downgrade the version by giving the version linked to the *main* section of the repository.
   With the example below (do the same with ``asterisk-dbg`` if applicable)::

    apt-get install asterisk='8:13.10.0-1~xivo9+2017.01+master+20170208.085934.3b143b7'

#. Restart the services (if you have a *XiVO CC*, you should restart its services too)::

    xivo-service restart


