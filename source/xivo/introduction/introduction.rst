************
Introduction
************

XiVO is a PABX application based on several free existant components including Asterisk and our own developments. XiVO provides a
solution for enterprises who wish to replace or add telephone services (PABX).

XiVO is free software. Most of its distinctive components, and XiVO as a whole, are distributed
under the GPLv3 license.

XiVO History
============

XiVO was created in 2005 by Sylvain Boily (Proformatique SARL). The XiVO mark is
now owned by Avencall SAS after a merge between Proformatique SARL and Avencall SARL
in 2010. The XiVO core team works for Proformatique INC in Quebec City since
2010, after Sylvain Boily moved to Quebec city.

XiVO 1.2 was released  on February 3, 2012.
