.. xivo-cc-doc Documentation master file.

*********************
XiVO-CC Documentation
*********************

.. figure:: logo_xivo-cc.png
   :scale: 30%


XiVO-CC is an application suite developed by Avencall_ Group, and provides enhancements of the XiVO_ PBX contact center functionalities.

.. _Avencall: http://www.xivo.solutions/
.. _XiVO: http://www.xivo.solutions/

Table of Contents
=================

.. toctree::
   :maxdepth: 3

   introduction/introduction
   install_and_config/index
   configuration/index
   features/index
   admin/admin

.. toctree::
   :maxdepth: 2

   xuc/index
   recording/index
   troubleshooting/index

.. toctree::
   :maxdepth: 2

   centralized_interface/index

Indices and tables
==================

* :ref:`genindex`
* :ref:`search`
