.. _web-assistant:

*************
Web Assistant
*************

The Web Assistant enables a user to:

* search contacts,
* call them,
* manage its favorites,
* manage its forward.


Login
=====

To login, one must have a user configured on the *XiVO PBX* with:

* XiVO Client enabled,
* Login, password and profile configured
* A configured line with a number

.. warning:: If a user tries to login without a line, an error message is displayed and
             user is redirected to the login page (this applies also to :ref:`desktop-assistant` )

.. figure:: errornoline.png
   :scale: 80%


Search
======

You can use the search section to lookup for people in the company:

.. figure:: search.png
   :scale: 100%

For this to work, one must configure the directories in the *XiVO PBX* as described in :ref:`directories` and :ref:`dird-integration-views`.


.. note:: Integration note: the *Web Assistant* support only the display of

  * 1 field for name (the one of type *name* in the directory display)
  * 3 numbers (the one of type *number* and the first two of type *callable*)
  * and 1 email


Favorites
=========

One can clic on the star to put a contact in its list of favorites.

For this to work, favorites must be configured in the *XiVO PBX* as described in :ref:`dird-favorites-configuration`.

Phone integration
=================

The *Web Assistant* can integrate with the phone to :

* call,
* put on hold,
* transfer
* etc.

For this feature to work one must use a :ref:`phone_integration_support` and follow the :ref:`phone_integration_installation` page.


WebRTC integration
==================

The *Web Assistant* can be used by users with WebRTC configuration, without physical phone.

For configuration and requirements, see :ref:`webrtc_requirements`.
