.. _desktop-assistant:

*****************
Desktop Assistant
*****************

What is the XiVO Desktop Assistant
==================================

The *XiVO Desktop Assistant* is the installable version of the  :ref:`web-assistant` application. This application offers the same features as `web-assistant`, i.e.:

* search contacts and show their presence, phone status
* make calls through your phone or using WebRTC
* access voicemail
* enable call forwards
* show the history of calls

Desktop application allows in addition:

* show popup when you receive call
* get keyboard shortcut to answer/hangup
* handle `callto:` and `tel:`` links. The assistant will automatically dial the number when you click on a link.

.. note:: You must already be logged in for automatic dialing to work, otherwise the assistant will just be started.


Navigation
==========

On first launch the application will display the settings page and ask for the xucmgt application address.

.. figure:: navigation.png
   :scale: 100%


The top menu allows you to navigate either to the application or to the settings page. If you did not enter any setting, the application will redirect you to the settings page.

About
-----

By clicking the **?** menu you will open a popup that show you technical information about the application that can be used to report bugs.


Settings
========

.. figure:: settings.png
   :scale: 100%

**Application Options**

* **Launch at startup** if enabled, the app starts automatically when you log in to your machine.
* **Close in tray** if enabled, the app stays running in the notification area after app window is closed.


**Global keyboard shortcut and Select 2 Call**

This field allow you to define one shortcut in application to handle all basic actions that can be done for managing calls. With one combination keypress you should be able to:

* **Call** if phone is available and your clipboard contains phone number (a.k.a **Select2Call**)
* **Answer** a call if phone is ringing
* **Hangup** if you are already calling someone

.. note::
  | To be able to **call** someone, you **must** beforehand have copied in your clipboard a phone number from any source (web page, e-mail, text document...)

   * **Linux**: select phone number then trigger `shortcut`
   * **Windows**:  select phone number, type ``Ctrl+C`` then trigger `shortcut`
   * **Mac**:  select phone number, type ``Cmd+C`` then trigger `shortcut`

  Default **Select2Call** shortcut is ``Ctrl+Space`` for **Windows** and **Linux** and ``Cmd+Space`` for **Mac**, you can either change it or disable it by leaving the field blank.

**Protocol and connection URL**

these two sections allows you to specify the protocol and address of the *xucmgt* application.

* Check **Secure** if you use *HTTPS* protocol to connect to the *xucmgt* application or check **Unsecure** otherwise.
* Enter the *xucmgt* application host address and port.


Update
======

On Windows, the application will check at startup for a new version of the application and offer to upgrade if one is available.

.. figure:: update.png
   :scale: 100%


On Debian, the update relies on the package manager behaviour. However you can check for any update by issuing the following commands:

.. code-block:: bash

                sudo apt-get update
                apt-cache policy xivo-desktop-assistant


Options
=======

The Desktop Assistant can be started with following options:

* -d to enable debug menu items
* --ignore-certificate-errors to disable certificate verification, this option is meant **only** for test purposes. You can use it with self-signed certificates.


WebRTC integration
==================

The *Desktop Assistant* can be used by users with WebRTC configuration, without physical phone.

For configuration and requirements, see :ref:`webrtc_requirements`.

To test your microphone and speaker, you may call the echo test application. After a welcome message, the application will
echo everything what you speak.

`*55` (echo test)
-------------------

1. Dial the `*55` number from your *Desktop Assistant*.
2. You should hear "Hello World" followed by the "Echo" announcement.
3. After the announcement, you will hear everything what you say.
4. Press `#` or hangup to exit the echo test application.

Using this application you may also get the latency between you and the server running the echo test.
