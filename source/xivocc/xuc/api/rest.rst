.. restapi:

########
Rest API
########


General form
============

   http://localhost:$xucport/xuc/api/1.0/$method/$domain/$username/

   withHeaders(("Content-Type", "application/json"))

   * $xucport : Xuc port number (default 8090)
   * $method : See available methods below
   * $domain : Represents a connection site, can be anything
   * $username : XiVO client user username

Events
======

  Xuc post JSON formated events on URL ``eventUrl = "http://localhost:8090/xivo/1.0/event/avencall.com/dropbox/"``
  configured in /usr/share/xuc/application.conf


Phone Event Notification
------------------------

Related to a username, phone event is in message payload same structure as javascript :ref:`phoneevents`

::

    {
      "username":"alicej",
      "message":{
         "msgType":"PhoneEvent",
         "ctiMessage":{"eventType":"EventDialing","DN":"1058","otherDN":"3000","linkedId":"1447670380.34","uniqueId":"1447670380.34","userData":{"XIVO_USERID":"9"}}}}


Connection
==========

   POST http://localhost:$xucport/xuc/api/1.0/connect/$domain/$username/

::

   {"password" : "password"}

    curl -XPOST -d '{"password":"<password>"}' -H "Content-Type: application/json"  http://localhost:8090/xuc/api/1.0/connect/avencall.com/<username>/

DND
===

   POST http://localhost:$xucport/xuc/api/1.0/dnd/$domain/$username/

::

   {"state" : [false|true]}


   curl -XPOST -d '{"state":false}' -H "Content-Type: application/json"  http://localhost:8090/xuc/api/1.0/dnd/avencall.com/<username>/

Dial
====
   POST http://localhost:$xucport/xuc/api/1.0/dial/$domain/$username/

::

   {"number" : "1101"}

   curl -XPOST -d '{"number":"<number>"}' -H "Content-Type: application/json"  http://localhost:8090/xuc/api/1.0/dial/avencall.com/<username>/

DialByUsername
==============
  POST http://localhost:$xucport/xuc/api/1.0/dialByUsername/$domain/$username/

::

{"username" : "john"}

curl -XPOST -d '{"username":"<username>"}' -H "Content-Type: application/json"  http://localhost:8090/xuc/api/1.0/dialByUsername/avencall.com/<username>/



DialFromQueue
=============
   POST http://localhost:$xucport/xuc/api/1.0/dialFromQueue/$domain/$username/

::

   {"destination":"1002","queueId":5,"callerIdName":"Thomas","callerIdNumber":"999999","variables":{"foo":"bar"}}

   curl -XPOST -d '{"destination":"1002","queueId":5,"callerIdName":"Thomas","callerIdNumber":"999999","variables":{"foo":"bar"}}' -H "Content-Type: application/json"  http://localhost:8090/xuc/api/1.0/dialFromQueue/avencall.com/<username>/

**Limitations:** Queue No Answer settings does not work - see :ref:`xivo-queues-no-answer`. Except: when there is no free Agent to queue (none attached, all Agents on pause or busy), then No answer settings work (but Fail does not).

.. note:: Line should be configured with enabled "Ring instead of On-Hold Music" enabled (on "Application: tab in queue configuration - see :ref:`xivo-queues`). Otherwise the queue will answers the call and the destination rings even if there are no agents available.

Phone number sanitization
-------------------------

Dial command automatically applies filters to the phone number provided to make it valid for Xivo. Especially, it removes invalid characters and handles properly different notations of international country code.

Some countries don't follow the international standard and actually keep the leading zero after the country code (e.g. Italy). Because of this, if the zero isn't surrounded by parenthesis, the filter keeps it [1]_.

.. [1] See `Redmine ticket #150 <http://redmine.avencall.com:3000/issues/150>`_

Forward
=======
All forward commands use the above payload

::

      {"state" : [true|false],
        "destination" : "1102")

Unconditionnal
--------------
   POST http://localhost:$xucport/xuc/api/1.0/uncForward/$domain/$username/

::

   curl -XPOST -d '{"state":true,"destination":"<destnb>"}' -H "Content-Type: application/json"  http://localhost:8090/xuc/api/1.0/uncForward/avencall.com/<username>/

On No Answer
------------
   POST http://localhost:$xucport/xuc/api/1.0/naForward/$domain/$username/

::

   curl -XPOST -d '{"state":true,"destination":"<destnb>"}' -H "Content-Type: application/json"  http://localhost:8090/xuc/api/1.0/naForward/avencall.com/<username>/

On Busy
-------
   POST http://localhost:$xucport/xuc/api/1.0/busyForward/$domain/$username/

::

   curl -XPOST -d '{"state":true,"destination":"<destnb>"}' -H "Content-Type: application/json"  http://localhost:8090/xuc/api/1.0/busyForward/avencall.com/<username>/

Handshake
=========
Will repost all events on the configured URL

   POST http://localhost:$xucport/xuc/api/1.0/handshake/$domain/

AgentLogout
===========
Logout un agent

    POST http://$xuchost:$xucport/xuc/api/1.0/agentLogout/

::

    curl -XPOST -d '{"phoneNumber":"<phoneNumber>"}' -H "Content-Type: application/json"  http://localhost:8090/xuc/api/1.0/agentLogout/

TogglePause
===========
Change state of an agent, pause if ready, ready if on pause

    POST http://$xuchost:$xucport/xuc/api/1.0/togglePause/

::

    curl -XPOST -d '{"phoneNumber":"<phoneNumber>"}' -H "Content-Type: application/json"  http://localhost:8090/xuc/api/1.0/togglePause/
