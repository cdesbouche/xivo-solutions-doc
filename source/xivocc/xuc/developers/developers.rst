.. highlightlang:: rest

.. _developper:

#########
Developer
#########

======================
Building and packaging
======================

Dependencies
------------

* Xivo Java Cti lib ; https://gitlab.com/xivo-utils/xivo-javactilib
    * mvn install
* theatrus/akka-quartz : https://github.com/theatrus/akka-quartz
    * sbt publish-local

(sudo apt-get install devscripts)

Update change log
-----------------

* dch -i in project root directory, parent of debian/changelog
* edit changelog to add version

Update documentation site
-------------------------

* update src/sphinx/conf.py with new version
* activator make-site
* copy target/sphinx/docs content to public/doc

Update xuc_logger.xml with new version
--------------------------------------

* Create debian package : activator debian:genChanges

=============
Docker
=============

Building docker image::

    activator docker:publish

    or

    activator docker:publishLocal

    docker tag xivo/xuc:2.4.32 xivo/xuc:latest


    activator clean test docker:publishLocal; docker tag -f xivo/xuc:1.9.0 xivo/xuc:latest;docker push xivo/xuc:1.9.0; docker push xivo/xuc:latest


========================
Documentation Guidelines
========================

The Xuc documentation uses `reStructuredText`_ as its markup language and is
built using `Sphinx`_.

.. _reStructuredText: http://docutils.sourceforge.net/rst.html
.. _sphinx: http://sphinx.pocoo.org

======
Sphinx
======

For more details see `The Sphinx Documentation <http://sphinx.pocoo.org/contents.html>`_

================
reStructuredText
================

For more details see `The reST Quickref <http://docutils.sourceforge.net/docs/user/rst/quickref.html>`_

Quick Reference

* http://docutils.sourceforge.net/docs/user/rst/cheatsheet.txt
* http://docutils.sourceforge.net/docs/user/rst/quickref.html
* http://openalea.gforge.inria.fr/doc/openalea/doc/_build/html/source/sphinx/rest_syntax.html

Sections
--------

Section headings are very flexible in reST. We use the following convention in
the Xuc documentation:

* ``#`` (over and under) for module headings
* ``=`` for sections
* ``-`` for subsections
* ``^`` for subsubsections
* ``~`` for subsubsubsections


Cross-referencing
-----------------

Sections that may be cross-referenced across the documentation should be marked
with a reference. To mark a section use ``.. _ref-name:`` before the section
heading. The section can then be linked with ``:ref:`ref-name```. These are
unique references across the entire documentation.

For example::

  .. _xuc-module:

  #############
   Xuc Module
  #############

  This is the module documentation.

  .. _xuc-section:

  Xuc Section
  ============

  Xuc Subsection
  ---------------

  Here is a reference to "xuc section": :ref:`xuc-section` which will have the
  name "Xuc Section".

Build the documentation
=======================

First install `Sphinx`_. See below.

Building
--------
For the html and pdf version of the docs::

    activator make-site

    open <project-dir>/target/sphinx/docs/index.html
    open <project-dir>/target/sphinx/docs/Xuc-doc.pdf

Installing Sphinx and other tools
---------------------------------

To be able to generate pdf and documentation you need install Sphinx and other tools::

   sudo easy_install -U Sphinx
   sudo apt-get install texlive-latex-base texlive-latex-recommended texlive-latex-extra texlive-fonts-recommended
