#############
Configuration
#############

This section covers specific configuration parameters for the different application of *XiVO CC*.


.. toctree::
   :maxdepth: 2

   agent
   ccmanager
   reporting
   webassistant
   webrtc
