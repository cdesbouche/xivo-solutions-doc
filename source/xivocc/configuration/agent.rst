.. _agent_configuration:

*****
Agent
*****

Recording
=========

Recording can be paused or started by an agent, this feature can be disabled by changing ``showRecordingControls`` option in file :file:`application.conf`.
You can also set the environnment variable ``SHOW_RECORDING_CONTROLS`` to false for your xucmgt container in :file:`/etc/docker/compose/custom.env` file. When disabled the recording status is not displayed any more

.. figure:: agent_recording.png
    :scale: 90%

Callbacks
=========

.. _agent_callbacks:

Callbacks panel can be removed using by changing ``showCallbacks`` option in :file:`application.conf`.
You can also use ``SHOW_CALLBACKS`` environment variable in :file:`/etc/docker/compose/custom.env` file.

.. figure:: agent-callbacks-view.png
    :scale: 90%

Queue control
=============

By using the ``showQueueControls`` option in application.conf, you may allow an agent to enter or leave a queue.
You can also use ``SHOW_QUEUE_CONTROLS`` environment variable in :file:`/etc/docker/compose/custom.env` file.

.. figure:: agent_queue.png
    :scale: 90%

Pause Cause and Status
======================

.. figure:: ccagentpausemenu.png
    :scale: 90%

By default the pause action from the agent cannot be specified with a specific cause such as Lunch Time, or Tea Time. To be able to use a specific cause,
you will have to define new Presences in the cti server configuration.

.. figure:: agentpausecauses.png
    :scale: 90%

You define presences with action **Activate pause to all queue** to **true**, for not ready causes,
and you must have one presence defined with an action **Disable pause to all queue** to be able to go back to not ready.
When this presences are defined, you must restart the xuc server to be able to use them in ccagent, these presences will also be
automatically available in :ref:`CCmanager <ccmanager>` and new real time counters will be calculated.

+---------------------------------+---------------------------------+
| Presence from ready to pause    |   Presence from pause to ready  |
+=================================+=================================+
|                                 |                                 |
|   .. figure:: readytopause.png  |  .. figure:: pausetoready.png   |
|       :scale: 70%               |      :scale: 70%                |
|                                 |                                 |
+---------------------------------+---------------------------------+


============
Screen Popup
============

It is possible to display customer information in an external web application using Xivo :ref:`sheet <custom-call-form>` mecanism.

* :menuselection:`Services > CTI Server > Sheets > Models`:

  * Tab *General Settings*: Give a name
  * Tab *Sheet*: You must define a sheet with two fields

    * ``folderNumber``

      * field type = ``text``
      * It has to be defined. Can be calculated or use a default value not equal to "-"
      * Note: You could leave "empty" using a whitespace (in hexadecimal: %20)

    * ``popupUrl``

      * field type = ``text``
      * The url to open when call arrives : i.e. http://mycrm.com/customerInfo?folder= the folder number will be automatically
        appended at the end of the URL

* :menuselection:`Services > CTI Server > Sheets > Events`: Choose the right events for opening the URL (if you choose two events, url will opened twice etc.)

Example : Using the caller number to open a customer info web page

* Define ``folderNumber`` with any default value i.e. 123456
* Define ``popupUrl`` with a display value of http://mycrm.com/customerInfo?nb={xivo-calleridnum}&fn= when call arrives web page http://mycrm.com/customerInfo?nb=1050&fn=123456 will be displayed

.. figure:: example_xivo_sheet.png
    :scale: 90%

.. _login_pause_funckeys:

Login and Pause management using function keys
==============================================

If agent uses phone with customizable :ref:`function keys <function_keys>`, these keys can be set to manage agent Login/Logout and Pause/Unpause.

* On on XiVO PBX edit ``/etc/xivo-xuc.conf`` and change variables:

  * ``XUC_SERVER_IP`` to IP address of XivoCC
  * ``XUC_SERVER_PORT`` to port of XUC Server (default is 8090)

* Configure user:

    * Open :menuselection:`Services > IPBX > IPBX settings > Users`
    * Edit the user, open *Func Keys* tab and add two rows
    * For Pause/Unpause function set *Type*: ``Customized``, *Destination*: ``***34`` followed by **agent number**, *Label*: ``Pause``, *Supervision*: ``Enabled``
    * For Login/Logout function set *Type*: ``Customized``, *Destination*: ``***30`` followed by **agent number**, *Label*: ``Login``, *Supervision*: ``Enabled``

.. figure:: agent_funckey.png
    :scale: 90%
