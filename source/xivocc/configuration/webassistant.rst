.. _webassistant_configuration:

*************
Web Assistant
*************

.. _webassistant_disable_webrtc:

Disabling WebRTC
================

WebRTC can be disabled globally by setting the ``DISABLE_WEBRTC`` environment varibale to ``true`` in :file:`/etc/docker/compose/custom.env` file.
