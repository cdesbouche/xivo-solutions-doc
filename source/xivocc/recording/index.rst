#########################
Recording server REST Api
#########################

This section describes the Recording server API.

In the following, all url are relative to the recording server base url and port. For example a relative URL ``/records/search`` is meant to be replaced by ``http://192.168.29.101:9400/records/search`` assuming the recording server is available on port ``9400`` at ``192.168.29.101``

Authentication
==============

To use the recording api you need to add an additional header in the HTTP Request. The header name is ``X-Auth-Token`` and its value must be the same as the ``authentication.token`` value in the ``application.conf`` of the recording server.

Example:

::

   curl -XPOST -H "Content-Type: application/json" -H "Accept: application/json" 
            -H 'X-Auth-Token: u@pf#41[gYHJm<]9N[a0iWDQQ7`e9k'  http://192.168.29.101:9400/records/search?pageSize=3  -d '{"agent": "1573"}'



Records
=======

Search
------

This api allows to search for recorded calls in the database using criteria.

**Description:**

:URL: ``/records/search``
:Method: ``POST``      
:Url parameters:
   :page: The page number to return, counting from 1
   :pageSize: The number of elements per page
:Request body: Json object with field & value pair.

----

**Allowed field names:**

:agent: The agent number to filter on
:queue: The queue number to filter on
:start: Return only calls starting or ending after the given value
:end: Return only calls starting before the given value
:callee: Return only calls with the given destination number
:caller: Return only calls with the give source number
:direction: Filter calls based on the call direction, one of ``incoming``, ``outgoing`` or ``all``
:key: filter based on the given key name in the attached data along with the value of the ``value`` field
:value: value of the key defined in the ``key`` field

----

**Example**

Query::
  
  curl -XPOST -H "Content-Type: application/json" -H "Accept: application/json" 
            -H 'X-Auth-Token: u@pf#41[gYHJm<]9N[a0iWDQQ7`e9k'  http://192.168.29.101:9400/records/search?pageSize=3  -d '{"agent": "1573"}'

Response::
  
    {
        "hasNext": true,
        "records": [
            {
                "agent": "Joe Dalton (1573)",
                "attached_data": {
                    "recording": "xivocc_gateway-1459433866.13971"
                },
                "dst_num": "73555",
                "duration": "00:00:20",
                "id": "xivocc_gateway-1459433866.13971",
                "queue": "oneforone (3555)",
                "src_num": "loadtester",
                "start": "2016-03-31 16:17:46",
                "status": "answered"
            },
            {
                "agent": "Joe Dalton (1573)",
                "attached_data": {
                    "recording": "xivocc_gateway-1459433330.13665"
                },
                "dst_num": "73555",
                "duration": "00:01:01",
                "id": "xivocc_gateway-1459433330.13665",
                "queue": "oneforone (3555)",
                "src_num": "loadtester",
                "start": "2016-03-31 16:08:51",
                "status": "answered"
            }
        ]
    }


Search by call id
-----------------

This api allows to search for a recorded call based on call id.

**Description:**

:URL: ``/records/callid_search``
:Method: ``POST``      
:Url parameters:
   :callid: The call id to retrieve

----

**Example**
Query::
  
  curl -XPOST -H "Accept: application/json" 
            -H 'X-Auth-Token: u@pf#41[gYHJm<]9N[a0iWDQQ7`e9k'  http://192.168.29.101:9400/records/callid_search?callid=1459435466.286075
            
Response::
             
        {
            "hasNext": false,
            "records": [
                {
                    "agent": "Dino Falconetti (1564)",
                    "attached_data": {
                        "recording": "xivocc_gateway-1459435465.15089"
                    },
                    "dst_num": "73556",
                    "duration": "",
                    "id": "xivocc_gateway-1459435465.15089",
                    "queue": "hotline (3556)",
                    "src_num": "loadtester",
                    "start": "2016-03-31 16:44:26",
                    "status": "answered"
                }
            ]
        }

Retrieve audio file
-------------------

This api allows to retrieve an audio file of a give call.

**Description:**

:URL: ``/records/records/<file-id>/audio``
:Method: ``GET``      
:Url parameters:
   :file-ud: The file to retrieve

----

**Example**::
  
  curl -XGET -H "Accept: application/json" -H 'X-Auth-Token: u@pf#41[gYHJm<]9N[a0iWDQQ7`e9k'  
            http://192.168.29.101:9400/records/xivocc_gateway-1459435465.15089/audio


Attach call data
----------------

This api allows to attach data to a given call

**Description:**

:URL: ``/call_data/<call-data-id>/attached_data``
:Method: ``POST``      
:Url parameters:
   :call-data-id: The id of the call-data, not to be confused by the call id or unique id.
:Request Body: An array of key value

----

**Example**::
  
  curl -XPOST -H "Content-Type: application/json" -H "Accept: application/json" -H 'X-Auth-Token: u@pf#41[gYHJm<]9N[a0iWDQQ7`e9k'  
            http://192.168.29.101:9400/call_data/761054/attached_data  -d '[{"key":"color", "value": "green"}]'

History
=======

Search
------

This api gives the call history of a given interface.

**Description:**

:URL: ``/history``
:Method: ``POST``      
:Url parameters:
   :size: The maximum number of result to return
:Request Body: A json object with a field named ``interface`` containing the interface to search for.

----

**Example**
Query::

  curl -XPOST -H "Content-Type: application/json" -H "Accept: application/json" -H 'X-Auth-Token: u@pf#41[gYHJm<]9N[a0iWDQQ7`e9k' http://localhost:9400/history  -d '{"interface":"SIP/az9kf7"}'

Response::

  [
   {
    "start": "2017-01-27 15:37:54",
    "duration": "00:00:18",
    "src_num": "1001",
    "dst_num": "3000",
    "status": "emitted",
    "src_firstname": "Poste",
    "src_lastname": "Poste 1001",
    "dst_firstname": null,
    "dst_lastname": "Cars"
   }
  ]

Last agent for number
---------------------

This api retrieves the last agent id who answered a given caller number.

**Description:**

:URL: ``/last_agent``
:Method: ``GET``      
:Url parameters:
   :callerNo: The calling number
   :since: The number of days to search in the history

----

**Example**
Query::

  curl -XGET -H "Content-Type: application/json" -H "Accept: application/json" -H 'X-Auth-Token: u@pf#41[gYHJm<]9N[a0iWDQQ7`e9k' 'http://localhost:9400/last_agent?callerNo=1002&since=100'

Response::

  {"agentNumber":"2000"}


