**************
REST API
**************

The XiVO Centralized Interface (XCI) exposes some REST API that you can use to integrate with your tools.

General form
============

   http://$my-server-ip:$xciport/api/1.0/$method
   
   withHeaders(("Content-Type", "application/json"))
   
   * $xciport : XCI port number (default 9001)
   * $method : See available methods below

Login
=====

A login request is required before subsequent API calls in order to get a session cookie.

	POST /api/1.0/login

Payload parameters :

``login`` (String)
	Login to connect with

``password`` (String)
	Password corresponding to the login

The server will return a cookie and you will be able to do other API calls. Example with CURL :

.. code-block:: bash

	curl 'http://localhost:9000/api/1.0/login' -H 'Content-Type: application/json' -c 'xci-cookie' --data-binary '{"login":"admin","password":"superpass"}'
	curl 'http://localhost:9000/api/1.0/xivo' -H 'Content-Type: application/json' -b 'xci-cookie'

XiVO
====

The following methods allow you to operate on the XiVOs managed by XCI.

List
----

List all the XiVOs configured on XCI.

	GET /api/1.0/xivo

.. code-block:: json

	{
	  "items": [
	    {
	      "id": 1,
	      "uuid": "8f159082-4b25-48b3-afec-1873491a60be",
	      "name": "xivo-220",
	      "host": "192.168.29.220",
	      "remainingSlots": 664
	    },
	    {
	      "id": 2,
	      "uuid": "15585b75-1d75-45b1-8678-520d1210ec59",
	      "name": "xivo-221",
	      "host": "192.168.29.221",
	      "remainingSlots": 280
	    }
	  ]
	}

Get
----

Get a XiVO by its ``id``.

	GET /api/1.0/xivo/$id

.. code-block:: json

	{
	  "id": 1,
	  "uuid": "8f159082-4b25-48b3-afec-1873491a60be",
	  "name": "xivo-220",
	  "host": "192.168.29.220",
	  "remainingSlots": 664
	}

Create
-------

Create a new XiVO.

	POST /api/1.0/xivo

Payload parameters :

``name`` (String)
	Display name of the XiVO

``host`` (String)
	Hostname or IP address of the XiVO

``configure`` (Boolean)
	If set to ``true``, XCI will immediately make the necessary configurations on the XiVO. If set to ``false``, it will only be added to XCI but not configured.

Synchronize configuration files
--------------------------------

	GET /api/1.0/xivo/synchronize_config_files

Entities
=========

The following methods allow you to operate on the entities made available by the XiVOS.

List
----

List all the entities available.

	GET /api/1.0/entities

.. code-block:: json

	{
	  "items": [
	    {
	      "id": 17,
	      "combinedId": "default@15585b75-1d75-45b1-8678-520d1210ec59",
	      "name": "default",
	      "displayName": "default",
	      "xivo": {
		"id": 2,
		"uuid": "15585b75-1d75-45b1-8678-520d1210ec59",
		"name": "xivo-221",
		"host": "192.168.29.221",
		"remainingSlots": 280
	      },
	      "intervals": [
		{
		  "start": "1700",
		  "end": "1799"
		},
		{
		  "start": "1961",
		  "end": ""
		},
		{
		  "start": "2600",
		  "end": "2799"
		}
	      ],
	      "presentedNumber": "inbNo"
	    },
	    {
	      "id": 22,
	      "combinedId": "default_analogique@15585b75-1d75-45b1-8678-520d1210ec59",
	      "name": "default_analogique",
	      "displayName": "default_analogique",
	      "xivo": {
		"id": 2,
		"uuid": "15585b75-1d75-45b1-8678-520d1210ec59",
		"name": "xivo-221",
		"host": "192.168.29.221",
		"remainingSlots": 280
	      },
	      "intervals": [
		{
		  "start": "3990000",
		  "end": "3999999"
		},
		{
		  "start": "39990000",
		  "end": "39999999"
		}
	      ],
	      "presentedNumber": "inbNo"
	    }
	  ]
	}

Get
----

Get an entity by its ``combinedId``.

	GET /api/1.0/entities/$combinedId

.. code-block:: json

	{
	  "id": 22,
	  "combinedId": "default_analogique@15585b75-1d75-45b1-8678-520d1210ec59",
	  "name": "default_analogique",
	  "displayName": "default_analogique",
	  "xivo": {
	    "id": 2,
	    "uuid": "15585b75-1d75-45b1-8678-520d1210ec59",
	    "name": "xivo-221",
	    "host": "192.168.29.221",
	    "remainingSlots": 280
	  },
	  "intervals": [
	    {
	      "start": "3990000",
	      "end": "3999999"
	    },
	    {
	      "start": "39990000",
	      "end": "39999999"
	    }
	  ],
	  "presentedNumber": "inbNo"
	}

.. _`Create entity`:

Create
-------

Create a new entity.

	POST /api/1.0/entities

Payload parameters :

``name`` (String)
	Name of the entity

``displayName`` (String)
	Displayed name of the entity

``xivoId`` (Integer)
	Id of the XiVO the entity will be attached to

``intervals`` (Array)
	Intervals of numbers this entity will support

	``start`` (String)
		Starting number of the interval

	``end`` (String)
		Ending number of the interval

``presentedNumber`` (String)
	Number to show on outgoing calls

Delete
-------

Delete an entity.

	DELETE /api/1.0/entities/$combinedId

Edit
-----

Edit an entity. See `Create entity`_ for fields details.

	PUT /api/1.0/entities/$combinedId

List users
----------

List users attached to an entity.

	GET /api/1.0/entities/$combinedId/users

.. code-block:: json

	{
	  "items": [
	    {
	      "id": 559,
	      "entity": {
		"id": 22,
		"combinedId": "default_analogique@15585b75-1d75-45b1-8678-520d1210ec59",
		"name": "default_analogique",
		"displayName": "default_analogique",
		"xivo": {
		  "id": 2,
		  "uuid": "15585b75-1d75-45b1-8678-520d1210ec59",
		  "name": "xivo-221",
		  "host": "192.168.29.221",
		  "remainingSlots": 280
		},
		"intervals": [
		  {
		    "start": "3990000",
		    "end": "3999999"
		  },
		  {
		    "start": "39990000",
		    "end": "39999999"
		  }
		],
		"presentedNumber": "inbNo"
	      },
	      "firstName": "Sous sol Logistique",
	      "lastName": "CLF 88:40 P3",
	      "internalNumber": "6260",
	      "externalNumber": "\"Sous sol Logistique CLF 88:40 P3\"",
	      "mail": null,
	      "ctiLogin": null,
	      "ctiPassword": null,
	      "provisioningNumber": "114133"
	    }
	  ]
	}

List available numbers
-----------------------

List available numbers for an entity

	GET /api/1.0/entities/$combinedId/available_numbers

.. code-block:: json

	{
	  "items": [
	    "3990000",
	    "3990001",
	    "3990002",
	    "3990003",
	    "3990004"
	  ]
	}

Users
=====

The following methods allow you to operate on the users made available by the XiVOS.

Get
----

Get a user by its ``id``.

	GET /api/1.0/users/$id

.. code-block:: json

	{
	  "id": 559,
	  "entity": {
	    "id": 22,
	    "combinedId": "default_analogique@15585b75-1d75-45b1-8678-520d1210ec59",
	    "name": "default_analogique",
	    "displayName": "default_analogique",
	    "xivo": {
	      "id": 2,
	      "uuid": "15585b75-1d75-45b1-8678-520d1210ec59",
	      "name": "xivo-221",
	      "host": "192.168.29.221",
	      "remainingSlots": 280
	    },
	    "intervals": [
	      {
		"start": "3990000",
		"end": "3999999"
	      },
	      {
		"start": "39990000",
		"end": "39999999"
	      }
	    ],
	    "presentedNumber": "inbNo"
	  },
	  "firstName": "Sous sol Logistique",
	  "lastName": "CLF 88:40 P3",
	  "internalNumber": "6260",
	  "externalNumber": null,
	  "mail": null,
	  "ctiLogin": null,
	  "ctiPassword": null,
	  "provisioningNumber": "114133"
	}

.. _`Create user`:

Create
-------

Create a new user.

	POST /api/1.0/users

Payload parameters :

``entityCId`` (String)
	Entity combinedId the user will be attached to

``templateId`` (Integer)
	Line template to apply to the user

``firstName`` (String)
	First name of the user

``lastName`` (String)
	Last name of the user

``internalNumber`` (String)
	Internal phone number of the user

``ctiLogin`` (String) *Optional*
	CTI login of the user

``ctiPassword`` (String) *Optional*
	CTI password of the user

Delete
-------

Delete a user.

	DELETE /api/1.0/users/$id

Edit
-----

Edit a user. See `Create user`_ for fields details.

	PUT /api/1.0/users/$id

Templates
==========

The following methods allow you to operate on the line templates used to create users.

List
----

List all the templates available.

	GET /api/1.0/templates

.. code-block:: json

	[
	  {
	    "id": 1,
	    "name": "Modèle 220",
	    "peerSipName": "auto",
	    "routedInbound": false,
	    "callerIdMode": "incomingNo",
	    "ringingTime": 30,
	    "voiceMailEnabled": false,
	    "voiceMailNumberMode": "short_number",
	    "xivos": [
	      1
	    ],
	    "entities": [
	      "default@8f159082-4b25-48b3-afec-1873491a60be"
	    ]
	  }
	]

Get
----

Get a template by its ``id``.

	GET /api/1.0/templates/$id

.. code-block:: json

	{
	  "id": 1,
	  "name": "Modèle 220",
	  "peerSipName": "auto",
	  "routedInbound": false,
	  "callerIdMode": "incomingNo",
	  "ringingTime": 30,
	  "voiceMailEnabled": false,
	  "voiceMailNumberMode": "short_number",
	  "xivos": [
	    1
	  ],
	  "entities": [
	    "default@8f159082-4b25-48b3-afec-1873491a60be"
	  ]
	}

.. _`Create template`:

Create
-------

Create a new template.

	POST /api/1.0/templates

Payload parameters :

``name`` (String)
	Name of the template

``xivos`` (Array of Integer)
	List of XiVOs ids the template will be available to

``entities`` (Array of String)
	List of entities combinedIds the template will be available to

``peerSipName`` (String)
	Possible values are ``auto`` or ``model``

``ringingTime`` (Integer)
	Number of seconds before incoming call is rejected

``routedInbound`` (Boolean)
	Whether or not the phone can be called from the outside

``routedInboundPrefix`` (String) *Compulsory if* ``routedInbound`` *is* ``true``
	SDA prefix to call the phone

``callerIdMode`` (String)
	Option specifying what number is displayed on outgoing call. Possible values are :
		* ``incomingNo`` : use the SDA prefix
		* ``anonymous`` : masked call
		* ``custom`` : a custom number

``customCallerId`` (String) *Compulsory if* ``callerIdMode`` *is* ``custom``
	Custom number to display on outgoing call

``voiceMailEnabled`` (Boolean)
	Whether or not to enable the voice mail

``voiceMailNumberMode`` (Boolean)
	Option specifying what number is used to call the voice mail. Possible values are :
		* ``short_number`` : use the default short number
		* ``custom`` : a custom number

``voiceMailCustomNumber`` (String) *Compulsory if* ``voiceMailNumberMode`` *is* ``custom``
	Custom number to call the voice mail

``voiceMailSendEmail`` (Boolean)
	Whether or not to send an email when a new message is left

Delete
-------

Delete a template.

	DELETE /api/1.0/templates/$id

Edit
-----

Edit a template. See `Create template`_ for fields details.

	PUT /api/1.0/templates/$id

Administrators
===============

The following methods allow you to operate on the administrators of the XCI.

List
----

List all the administrators present.

	GET /api/1.0/administrators

.. code-block:: json

	{
	  "items": [
	    {
	      "id": 1,
	      "login": "admin",
	      "name": "",
	      "password": "+\/\/rIncoyp\/Ai\/8l3xSEeSY+P+x4uNle7cHkL6rpPS3ucgr2EAJIqnQbsIpSGwHj",
	      "superAdmin": true,
	      "ldap": false,
	      "entities": [
		
	      ]
	    }
	  ]
	}

Get
----

Get an administrator by its ``id``.

	GET /api/1.0/administrators/$id

.. code-block:: json

	{
	  "id": 1,
	  "login": "admin",
	  "name": "",
	  "password": "+\/\/rIncoyp\/Ai\/8l3xSEeSY+P+x4uNle7cHkL6rpPS3ucgr2EAJIqnQbsIpSGwHj",
	  "superAdmin": true,
	  "ldap": false,
	  "entities": [
	    
	  ]
	}

.. _`Create administrator`:

Create
-------

Create a new administrator.

	POST /api/1.0/administrators

Payload parameters :

``login`` (String)
	Login of the administrator

``name`` (String)
	Displayed name of the administrator

``ldap`` (Boolean)
	Whether or not to use the LDAP authentication configured in ``application.conf``

``password`` (String) *Compulsory if* ``ldap`` *is* ``false``
	Password used by the administrator to login

``superAdmin`` (Boolean)
	Whether or not this administrator is a super-administrator. Super-administrators can manage everything in XCI.

``entityIds`` (Array of Integer)
	List of entities this administrator has the rights to manage

Delete
-------

Delete an administrator.

	DELETE /api/1.0/administrators/$id

Edit
-----

Edit an administrator. See `Create administrator`_ for fields details.

	PUT /api/1.0/administrators/$id

Example (Python 3)
==================

.. code-block:: python

	#!/usr/bin/env python3
	# -*- coding: utf-8 -*-

	from urllib.parse import urlencode
	from urllib.request import Request, urlopen
	import json, sys

	class XCIApiExample:
		base_url = None
		cookie = None

		def __init__(self, base_url, login, password):
			self.base_url = base_url
			self.make_login(login, password)

		def make_login(self, login, password):
			data = {"login": login, "password": password}
			response = self.make_post_request("/login", data)
			self.cookie = response.info()["Set-Cookie"]

		def get_entities(self):
			response = self.make_get_request("/entities")
			return self.handle_json_response(response)

		def get_available_numbers(self, entity):
			response = self.make_get_request("/entities/" + entity["combinedId"] + "/available_numbers")
			return self.handle_json_response(response)

		def create_line_template(self, data):
			self.make_post_request("/templates", data)

		def get_line_templates(self):
			response = self.make_get_request("/templates")
			return self.handle_json_response(response)

		def create_user(self, data):
			self.make_post_request("/users", data)

		def make_get_request(self, method):
			request = Request(self.base_url + method, headers = {"Cookie": self.cookie})
			response = urlopen(request)
			return response

		def make_post_request(self, method, data):
			header = {"Content-Type": "application/json", "Cookie": self.cookie if self.cookie else ""}
			request = Request(self.base_url + method, json.dumps(data).encode(), header)
			response = urlopen(request)
			return response

		def handle_json_response(self, response):
			return json.loads(response.read().decode())


	# Initialize API
	api_example = XCIApiExample("http://192.168.29.103:9001/api/1.0", "admin", "superpass")

	# Get an entity and its XiVO
	entities = api_example.get_entities()["items"]
	if (len(entities) == 0):
		sys.exit("There isn't any XiVO configured yet or they don't have any entity !")
	else:
		entity = entities[1]
		xivo = entity["xivo"]
		print("Selected entity \"%s\" in XiVO \"%s\""%(entity["name"], xivo["name"]))

	# Create a line template
	template_data = {
		"name": "My line template",
		"xivos": [xivo["id"]],
		"entities": [entity["combinedId"]],
		"peerSipName": "auto",
		"ringingTime": 30,
		"routedInbound": False,
		"callerIdMode": "anonymous",
		"voiceMailEnabled": False
	}
	api_example.create_line_template(template_data)
	line_template = api_example.get_line_templates()[0]
	print("New line template created")

	# Create a user
	user_data = {
		"entityCId": entity["combinedId"],
		"templateId": line_template["id"],
		"firstName": "Alice",
		"lastName": "In Wonderland",
		"internalNumber": api_example.get_available_numbers(entity)["items"][0]
	}	
	api_example.create_user(user_data)
	print("New user created")


