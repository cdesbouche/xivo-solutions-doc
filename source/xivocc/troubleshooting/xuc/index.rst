###############################################################
Xuc et Xuc_mgt - applications web ccmanager, agent et assistant
###############################################################

Basic checks
============

XUC overview page
-----------------
XUC overview page available at @XUC_IP:PORT, usually @SERVER_IP:8090. You have to check if the "Internal configuration cache database"
contains agents, queues etc.

XUC sample page
---------------

XUC sample page available at @XUC_IP:PORT/sample, usually @SERVER_IP:8090/sample. You can use this page to check user login and other
API functions. CCManager, agent and assistant web use functions available on the sample page.

Web assistant
=============

Favorites
---------

With *XiVO PBX* < 15.18 (without xivo-dird) one has to configure xuc with the runtime parameter ``xivo.useDird=false`` : with this parameter set, one
can't add Favorites (though the star to add a Favorite is visible).

