.. XiVO-doc Documentation master file.

**************************************
XiVO Solutions |version| Documentation
**************************************

XiVO Solutions is a complete solution for entreprise communications and contact centre infrastructure.
XiVO is a PABX application, XiVO-CC is an application suite for contact centers.

Table of Contents
=================

.. toctree::
   :maxdepth: 1

   xivo/index
   xivocc/index
   releasenotes/index


Indices and tables
==================

* :ref:`genindex`
* :ref:`search`
